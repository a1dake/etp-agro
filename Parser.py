import sys
import requests
import cfscrape
import urllib3
from urllib3 import exceptions
import sqlite3
import os
from datetime import datetime
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "etpagro.db")
sys.path.append(db_path)
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def create_database(self):
        try:
            conn = sqlite3.connect(db_path)
            sql = conn.cursor()
            sql.execute("""CREATE TABLE orders (url text);""")
            conn.close()
        except:
            pass

    def insert_data(self, value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("INSERT INTO orders(url) VALUES (?)", [value])
        conn.commit()
        conn.close()

    def search_data(self, search_value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("SELECT * FROM orders WHERE url = ?", (search_value,))
        rows = c.fetchall()
        if rows:
            conn.close()
            return True
        else:
            conn.close()
            return False

    def get_ads(self):
        ads = []
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://zakupka.etpagro.ru/api/wf-data/procedure/list-public?id=&query=procedure%2Flist-public&withState=false' \
              '&variables=%7B%22rows%22%3A100%2C%22first%22%3A0%2C%22name%22%3Anull%2C%22number%22%3Anull%2C%22procedureTypeCode%22%3Anull' \
              '%2C%22organizer%22%3Anull%2C%22customer%22%3Anull%2C%22state%22%3A%5B%22await_trade%22%2C%22request_review%22%2C%22trade%22' \
              '%2C%22accepting_requests%22%2C%22results%22%5D%2C%22region%22%3Anull%2C%22okpd2%22%3A%5B%5D%2C%22datePublicationFrom%22%3Anull' \
              '%2C%22datePublicationTo%22%3Anull%2C%22dateApplicationDeadlineFrom%22%3Anull%2C%22dateApplicationDeadlineTo%22%3Anull%7D'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://etpagro.ru/', verify=False)

        response = session.get(url, verify=False)
        data = response.json()

        for item in data.get('data').get('pagination').get('items'):
            ads.append(item.get('id'))
        return ads

    def get_data(self):
        self.create_database()
        ads = self.get_ads()
        data = []
        for tender_id in ads:
            db_search = self.search_data(tender_id)
            if db_search:
                continue
            else:
                content = self.get_content(tender_id)
                data.append(content)
                self.insert_data(tender_id)
        return data

    def get_content(self, tender_id):
        content = {}
        try:
            session = requests.Session()
            url = f'https://zakupka.etpagro.ru/api/view/procedure-proposal-request/view-public?id={tender_id}&type=gui&part='
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36'
            })
            response = session.get(url, verify=False)
            data = response.json()
            view_data = data.get('view').get('@preload').get('procedure')

            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': '',
                    'scoringDate': '',
                    'biddingDate': ''
                },

                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': '',
                    'contactPhone': '',
                    'contactEMail': '',
                }
            }
            item_data['title'] = str(self.get_title(view_data))
            item_data['url'] = str(self.get_url(view_data))

            item_data['purchaseType'] = str(self.get_type(view_data))
            item_data['purchaseNumber'] = str(self.get_number(view_data))

            item_data['customer']['fullName'] = str(self.get_customer(view_data))
            item_data['customer']['factAddress'] = str(self.get_adress(view_data))

            item_data['procedureInfo']['endDate'] = self.get_end_date(view_data)
            item_data['procedureInfo']['scoringDate'] = self.get_scor_date(view_data)

            last_name, first_name, middle_name = self.get_contact(view_data)
            item_data['contactPerson']['lastName'] = str(last_name)
            item_data['contactPerson']['firstName'] = str(first_name)
            item_data['contactPerson']['middleName'] = str(middle_name)

            item_data['contactPerson']['contactPhone'] = str(self.get_phone(view_data))
            item_data['contactPerson']['contactEMail'] = str(self.get_email(view_data))

            item_data['lots'] = self.get_lots(view_data)
            item_data['attachments'] = self.get_docs(tender_id)

            content = item_data
        except Exception as e:
            print(e)
            print("Страница с ошибкой ", tender_id)

        return content

    def get_title(self, view_data):
        try:
            data = view_data.get('name')
        except Exception as e:
            print(e)
            data = ''
        return data

    def get_type(self, view_data):
        try:
            data = view_data.get('procedureType').get('name')
        except:
            data = ''
        return data

    def get_number(self, view_data):
        try:
            data = view_data.get('number')
        except:
            data = ''
        return data

    def get_url(self, view_data):
        try:
            data = view_data.get('_id')
            url = 'https://zakupka.etpagro.ru/procedure-proposal-request/view-public?id=' + data
        except:
            url = ''
        return url

    def get_contact(self, view_data):
        name_data = view_data.get("executorInfo").get('person').split()
        try:
            last_name = name_data[0]
        except:
            last_name = ''
        try:
            first_name = name_data[1]
        except:
            first_name = ''
        try:
            middle_name = name_data[2]
        except:
            middle_name = ''

        return last_name, first_name, middle_name

    def get_phone(self, view_data):
        try:
            data = view_data.get("executorInfo").get('phone').get('num')
        except:
            data = ''
        return data

    def get_email(self, view_data):
        try:
            data = view_data.get("executorInfo").get('email')
        except:
            data = ''
        return data

    def get_customer(self, view_data):
        try:
            data = view_data.get('organizerInfo').get('name')
        except:
            data = ''
        return data

    def get_adress(self, view_data):
        try:
            data = view_data.get('organizerInfo').get('factualAddress')
        except:
            data = ''
        return data

    def get_end_date(self, view_data):
        try:
            old_date = view_data.get('dates').get('applicationDeadline')
            date = self.formate_date(old_date)
        except:
            date = None
        return date

    def get_scor_date(self, view_data):
        try:
            old_date = view_data.get('dates').get('summingEnd')
            date = self.formate_date(old_date)
        except:
            date = None
        return date

    def get_lots(self, view_data):
        data = []
        lots = {
            'region': '',
            'address': '',
            'price': '',
            'lotItems': []

        }
        names = {'code': '1',
                 'name': ' '.join(view_data.get('lots')[0].get('name').split())}
        lots['lotItems'].append(names)

        lots['region'] = str(view_data.get('region').get('name'))
        data.append(lots)
        return data

    def get_docs(self, tender_id):
        docs = []
        session = requests.Session()
        url = f'https://zakupka.etpagro.ru/api/data/procedure-proposal-request/view-public?id={tender_id}&query=procedure%2Fdocument%2Flist&variables=%7B%22id%22%3A%22{tender_id}%22%7D'
        session.headers.update({
            'Accept': 'accept: application/json, text/plain, */*',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.61'
        })

        response = session.get(url, verify=False)
        view_data = response.json()
        try:
            for doc in view_data.get('data').get('pagination').get('items'):
                doc_list = {'docDescription': str(doc.get('name')), 'url': 'https://zakupka.etpagro.ru/' + str(doc.get('_id'))}
                docs.append(doc_list)
        except:
            pass
        return docs

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%Y-%m-%dT%H:%M:%S.%fZ')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
